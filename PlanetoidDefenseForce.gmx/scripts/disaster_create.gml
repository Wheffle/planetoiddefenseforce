///disaster_create(disaster_type);
{
    var planet = noone;
    with(obj_planet) planet = id;

    switch(argument0)
    {
        // *** FIRE ***
        case disaster.fire:
            var list = ds_list_create();
            with(radial_interact)
            {
                if (flammable) ds_list_add(list, id);
            }
            
            if (ds_list_size(list) > 0)
            {
                ds_list_shuffle(list);
                var i = list[| 0];
                with(i)
                {
                    instance_create_radial(planet, pos_deg, pos_r, obj_fire);
                    instance_destroy();
                }
                
                //set emergency message
                var msg = "A fire has started! Find it and put it out before it spreads! Fires are much less dangerous in low oxygen environments.";
                set_emergency_message(msg);
            }
            
            ds_list_destroy(list);           
        break;
        
        // *** VOLCANO ***
        case disaster.volcano:
            var deg = irandom(360);
            var v = instance_create_radial(planet, deg, 0, obj_lava);
            with(radial_interact)
            {
                if (flammable and place_meeting(x, y, v))
                {
                    instance_destroy();
                }   
            }
            
            //effects
            set_screen_shake(5);
            
            repeat(10)
            {
                psys_create_puff(v.x, v.y, c_red);
            }
            
            repeat(5)
            {
                psys_create_smoke(v.x-10+irandom(20), v.y-10+irandom(20), c_maroon);
            }
            
            with(planet)
            {
                if (surface_exists(surf))
                {
                    surface_set_target(surf);
                    var xx = surf_half+lengthdir_x(surf_half-5, v.pos_deg);
                    var yy = surf_half+lengthdir_y(surf_half-5, v.pos_deg);
                    draw_sprite_ext(s_cooledlava, 0, xx, yy, 1, 1, v.image_angle, c_white, 1);
                    surface_reset_target();
                }
            }
            
            //set emergency message
            var msg = "A volcano has erupted! Nearby plants and machines may have been destroyed or caught fire! The resulting lava pool will be dangerous until it cools.";
            set_emergency_message(msg);
        break;
        
        // *** METEOR ***
        case disaster.meteor:
            var deg = irandom(360);
            instance_create_radial(planet, deg, 0, obj_meteor);
        
            //set emergency message
            var msg = "Meteor impact imminent! It may destroy surface objects and cause fires where it lands!";
            set_emergency_message(msg);
        break;
        
        // *** BUG ***
        case disaster.bug:
            repeat(3)
            {
                var deg = irandom(360);                
                var bug = instance_create_radial(planet, deg, 0, obj_bug);
                repeat(5) psys_create_smoke(bug.x-20+random(40), bug.y-20+random(40), global.c_brown);
            }
        
            //set emergency message
            var msg = "Insect-like dangerous pests have been detected devouring plants! Were they some kind of dormant native life? Stop them before they destroy the ecosystem!";
            set_emergency_message(msg);        
        break;
        
        // *** WEED ***
        case disaster.weed:
            var weed_grew = false;
            repeat(2)
            {        
                var tries = 20;        
                repeat (tries)
                {
                    var deg = irandom(360);
                    var w = spawn_killer_weed(planet, deg);
                    if (w != noone)
                    { 
                        weed_grew = true;
                        break;
                    }
                }
            }
                        
            if (weed_grew)
            {
                //set emergency message
                var msg = "A strange alien plant has been detected growing on the surface. It is extremely hostile and seems to be able to spread very quickly, choking out other plants!";
                set_emergency_message(msg);  
            }
        break;
    }
}
