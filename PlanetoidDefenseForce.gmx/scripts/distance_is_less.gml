///bool = distance_is_less(x1, y1, x2, y2, dist, equal_is_true);
{
    var xx = argument2 - argument0;
    var yy = argument3 - argument1;
    var input_sq = argument4*argument4;
    var dist_sq= xx*xx + yy*yy;
    if (dist_sq < input_sq) return true;
    if (argument5 and dist_sq == input_sq) return true;
    return false;
}
