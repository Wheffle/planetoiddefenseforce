///draw_planet_stats()
/*
    Designed to be executed in draw_gui of obj_planet.
*/
{
    var w = 200;
    var h = 128;
    var w2 = w div 2;

    var x1 = view_wview[0]-w-2;
    var y1 = 2;
    var x2 = x1+w-1;
    var y2 = y1+h-1;
    
    //draw border
    draw_set_color(c_white);
    draw_rectangle(x1-1, y1-1, x2+1, y2+1, true);
    draw_rectangle(x1-2, y1-2, x2+2, y2+2, true);
    
    //draw_background
    draw_set_alpha(0.5);
    draw_set_color(c_black);
    draw_rectangle(x1, y1, x2, y2, false);
    draw_set_alpha(1);
    
    //draw title
    draw_set_font(font_12b);
    draw_set_color(c_lime);
    draw_set_halign(fa_center);
    draw_text(x1+w2, y1+2, "Planet");
    
    //draw_resources
    var xx = x1+4;
    var yy = y1+22;
    draw_set_halign(fa_left);
    draw_set_font(font_12);
    draw_set_color(c_white);
    
    draw_text(xx, yy, "Atmosphere:");
    draw_text(xx, yy+16, "Nitrogen:");
    draw_text(xx, yy+32, "Oxygen:");
    draw_text(xx, yy+48, "C02:");
    draw_text(xx, yy+64, "Moisture:");
    draw_text(xx, yy+80, "Ecosystem:");
    
    draw_set_color(c_yellow);
    draw_text(xx+w2+32, yy, string_percent(atmos));
    draw_text(xx+w2+32, yy+16, string_percent(nitrogen));
    draw_text(xx+w2+32, yy+32, string_percent(oxygen));
    draw_text(xx+w2+32, yy+48, string_percent(co2));
    draw_text(xx+w2+32, yy+64, string_percent(moisture));
    draw_text(xx+w2+32, yy+80, string_percent(flora));
}
