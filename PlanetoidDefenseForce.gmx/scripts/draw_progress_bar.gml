///draw_progress_bar(x, y, w, h, prog, bar_color);
{
    var x1 = argument0;
    var y1 = argument1;
    var x2 = x1 + argument2 - 1;
    var y2 = y1 + argument3 - 1;
    
    draw_set_color(c_white);
    draw_rectangle(x1-1, y1-1, x2+1, y2+2, true);
    
    draw_set_color(c_black);
    draw_rectangle(x1, y1, x2, y2, false);
    
    if (argument4 > 0)
    {
        draw_set_color(argument5);
        draw_rectangle(x1+2, y1+2, x1+2+(argument2-4)*argument4, y2-2, false);
    }
}
