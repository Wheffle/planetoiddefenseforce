///draw_resources();
/*
    Designed to be executed in draw_gui event of obj_player.
*/
{
    var w = 200;
    var h = 64;
    var w2 = w div 2;

    var x1 = 2;
    var y1 = 78;
    var x2 = x1+w-1;
    var y2 = y1+h-1;
    
    //draw border
    draw_set_color(c_white);
    draw_rectangle(x1-1, y1-1, x2+1, y2+1, true);
    draw_rectangle(x1-2, y1-2, x2+2, y2+2, true);
    
    //draw_background
    draw_set_alpha(0.5);
    draw_set_color(c_black);
    draw_rectangle(x1, y1, x2, y2, false);
    draw_set_alpha(1);
    
    //draw_title
    draw_set_font(font_12b);
    draw_set_color(c_white);
    draw_set_halign(fa_center);
    draw_text(x1+w2, y1+2, "Backpack");
    
    //draw_resources
    var xx = x1+4;
    var yy = y1+22;
    draw_set_halign(fa_left);
    draw_set_font(font_12);
    draw_set_color(c_white);
    
    draw_text(xx, yy, "Metal:");
    draw_text(xx, yy+16, "Biomass:");
    
    draw_set_color(c_yellow);
    draw_text(xx+w2, yy, string(metal));
    draw_text(xx+w2, yy+16, string(biomass));
}
