///draw_ship_timer();
/*
    Designed to be executed in draw_gui event of ctrl_game.
*/
{
    var w = 200;
    var h = 72;
    var w2 = w div 2;

    var x1 = 2;
    var y1 = 2;
    var x2 = x1+w-1;
    var y2 = y1+h-1;
    
    //draw border
    draw_set_color(c_white);
    draw_rectangle(x1-1, y1-1, x2+1, y2+1, true);
    draw_rectangle(x1-2, y1-2, x2+2, y2+2, true);
    
    //draw_background
    draw_set_alpha(0.5);
    draw_set_color(c_black);
    draw_rectangle(x1, y1, x2, y2, false);
    draw_set_alpha(1);
    
    //draw_title
    draw_set_font(font_12b);
    draw_set_color(c_yellow);
    draw_set_halign(fa_center);
    draw_text(x1+w2, y1+2, "Colony Ship");
    
    //draw progress bar
    var xx = x1+4;
    var yy = y1+22;
    draw_set_halign(fa_left);
    draw_set_font(font_12);
    draw_set_color(c_white);   
    draw_text(xx, yy, "time until arrival:");
    
    draw_progress_bar(xx, yy+24, w-8, 16, end_timer/end_timer_set, c_yellow);
}
