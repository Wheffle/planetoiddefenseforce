///draw_tooltip(x, y, title, text, width);
{
    //get deminsions
    draw_set_font(font_12);
    var w = 8 + string_width_ext(argument3, -1, argument4);
    var h = 12 + string_height(argument2) + string_height_ext(argument3, -1, argument4);
          
    var x1 = argument0;
    var y1 = argument1;
    var x2 = x1 + w - 1;
    var y2 = y1 + h - 1;
    var x_center = x1 + (w div 2);
    
    //draw border
    draw_set_color(c_white);
    draw_rectangle(x1-1, y1-1, x2+1, y2+1, true);
    draw_rectangle(x1-2, y1-2, x2+2, y2+2, true);
    
    //draw background
    draw_set_alpha(0.5);
    draw_set_color(c_black);
    draw_rectangle(x1, y1, x2, y2, false);
    draw_set_alpha(1);
    
    //draw title
    draw_set_font(font_12b);
    draw_set_halign(fa_center);
    draw_set_color(c_white);
    draw_text(x_center, y1+4, argument2);
    
    //draw body
    draw_set_font(font_12);
    draw_set_halign(fa_left);
    draw_text_ext(x1+4, y1+24, argument3, -1, w-8); 
}
