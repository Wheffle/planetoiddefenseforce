///draw_tutorial(text, color);
{
    //get deminsions
    var max_w = 500;
    draw_set_font(font_12);
    var w = 8 + string_width_ext(argument0, -1, max_w);
    var w2 = w div 2;
    var h = 8 + string_height_ext(argument0, -1, max_w);
          
    var xx = view_wview[0] div 2;
    
    var x1 = xx - w2;
    var y1 = 4;
    var x2 = x1 + w - 1;
    var y2 = y1 + h - 1;
    
    //draw border
    draw_set_color(argument1);
    draw_rectangle(x1-1, y1-1, x2+1, y2+1, true);
    draw_rectangle(x1-2, y1-2, x2+2, y2+2, true);
    
    //draw background
    draw_set_alpha(0.5);
    draw_set_color(c_black);
    draw_rectangle(x1, y1, x2, y2, false);
    draw_set_alpha(1);
    
    //draw text
    draw_set_font(font_12);
    draw_set_color(argument1);
    draw_text_ext(x1+4, y1+4, argument0, -1, w-8); 
}
