///id = instance_create_radial(planet, pos_deg, pos_r, object_index);
{
    var i = instance_create(0, 0, argument3);
    with(i)
    {
        planet = argument0;
        pos_deg = argument1;
        pos_r = argument2;
        radial_place();
    }
    return i;
}
