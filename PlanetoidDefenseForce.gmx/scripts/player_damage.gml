///player_damage(source_deg, stun_timer_set);
{
    with(obj_player)
    {
        if (!stunned and stun_invincible <= 0)
        {
            stunned = true;
            stun_timer = argument1;
            
            v_spd = 4;
            
            var source_deg = argument0 - pos_deg;
            while(source_deg < 0) source_deg += 360;
            while(source_deg > 360) source_deg -= 360;
            if (source_deg < 180)
            {
                stun_dir = 1;
            }
            else
            {
                stun_dir = -1;
            }
        }
    }
}
