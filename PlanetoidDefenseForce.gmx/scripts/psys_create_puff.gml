///psys_create_puff(x, y, color);
{
    with(ctrl_game)
    {
        part_type_colour1(p_puff, argument2);
        part_type_size(p_puff, 0.1, 0.2, 0, 0);
        part_particles_create(psys, argument0, argument1, p_puff, 1);
    }
}
