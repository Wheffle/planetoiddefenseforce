///psys_create_smoke(x, y, color);
{
    with(ctrl_game)
    {
        part_type_colour1(p_smoke, argument2);
        var dir = 0;
        with(planet) dir = point_direction(x, y, argument0, argument1);
        part_type_direction(p_smoke, dir, dir, 0, 0);
        part_particles_create(psys, argument0, argument1, p_smoke, 1);
    }
}
