///psys_setup;
/*
    Designed to be executed in the ctrl_game object to set up the particle system.
*/
{
    part_system_depth(psys, -100);

    p_puff = part_type_create();
    part_type_sprite(p_puff, s_flare, false, false, false);
    part_type_alpha2(p_puff, 1, 0);
    part_type_life(p_puff, 60, 90);
    part_type_direction(p_puff, 0, 360, 0, 0);
    part_type_speed(p_puff, 0.5, 1, -0.01, 0);
    
    p_smoke = part_type_create();
    part_type_sprite(p_smoke, s_flare, false, false, false);
    part_type_size(p_smoke, 0.2, 0.25, 0, 0);
    part_type_alpha2(p_smoke, 1, 0);
    part_type_life(p_smoke, 60, 90);
    part_type_speed(p_smoke, 0.3, 0.6, 0, 0);
}
