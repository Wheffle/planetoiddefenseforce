///radial_move_h(distance);
/*
    Designed to be executed within the scope of a "obj_radial" instance or child
*/
{
    var full_r = pos_r + planet.radius;
    
    var delta_deg = darctan(argument0/full_r);
    pos_deg += delta_deg*-1;
    if (pos_deg < 0) pos_deg += 360;
    if (pos_deg > 360) pos_deg -= 360;
    
    radial_place();
}
