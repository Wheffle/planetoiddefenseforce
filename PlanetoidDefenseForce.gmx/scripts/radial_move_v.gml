///radial_move_v(distance);
/*
    Designed to be executed within the scope of a "obj_radial" instance or child
*/
{
    pos_r += argument0;
    radial_place();
}
