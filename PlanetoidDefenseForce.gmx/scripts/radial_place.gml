///radial_place();
/*
    Designed to be executed within the scope of a "obj_radial" instance or child
*/
{
    x = planet.x + lengthdir_x(planet.radius + pos_r, pos_deg);
    y = planet.y + lengthdir_y(planet.radius + pos_r, pos_deg);
    image_angle = pos_deg - 90;
}
