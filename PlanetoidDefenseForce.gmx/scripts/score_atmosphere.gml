///string = score_atmosphere(score);
{
    if (argument0 > 0.90)
    {
        return "The atmosphere is thick and comfortable.";
    }
    if (argument0 > 0.70)
    {
        return "The atmosphere is a little thin but reasonable.";
    }
    if (argument0 > 0.40)
    {
        return "The atmosphere is not ideal but survivable.";
    }
    
    return "The atmosphere is too thin to survive in without a pressure suit."

}
