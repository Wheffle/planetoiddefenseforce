///string = score_co2(score);
{
    if (argument0 > 0.25)
    {
        return "The heavy levels of CO2 make the air downright poisonous. However, dry ice and sparkling water will be abundant.";
    }
    if (argument0 > 0.15)
    {
        return "The air won't kill you instantly from CO2 poisoning, but it won't take long.";
    }
    if (argument0 > 0.07)
    {
        return "Running marathons outdoors without a mask will be recommended against due to CO2 levels.";
    }
    if (argument0 > 0.02)
    {
        return "The CO2 levels are reasonable, though manufacturing soda-pop will be just as hard as it is on Earth.";
    }
    
    return "There's hardly any carbon dioxide in the air. So no global warming, but don't plants need that stuff?"

}
