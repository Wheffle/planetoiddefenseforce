///score_create();
{
    var s = instance_create(0, 0, obj_score);
    with(s)
    {
        //get planet stats
        with(obj_planet)
        {
            other.flora = flora;
            other.atmos = atmos;            
            other.moisture = moisture;
            other.nitrogen = nitrogen;
            other.oxygen = oxygen;
            other.co2 = co2;
        }
        
        if (instance_exists(obj_fire)) has_fire = true;
        if (instance_exists(obj_killerweed)) has_weed = true;
        
        //final score
        var adjust_oxygen = oxygen;
        if (adjust_oxygen > 0.20) adjust_oxygen = 0.20;
        
        var adjust_co2 = co2;
        adjust_co2 -= 0.07;
        if (adjust_co2 < 0) adjust_co2 = 0;
        
        score_final = 
            flora +
            atmos +
            moisture +
            (adjust_oxygen/0.20) +
            (1 - adjust_co2);
            
        score_final /= 5;            
        
        //set up score message
        score_msg = 
            "Atmosphere: " + string_percent(atmos) +
            "#Oxygen: " + string_percent(oxygen) +
            "#CO2: " + string_percent(co2) + 
            "#Moisture: " + string_percent(moisture) + 
            "#Ecosystem: " + string_percent(flora) + "#";
            
        score_msg +=
            "#"+score_atmosphere(atmos)+" "+score_oxygen(oxygen)+" "+score_co2(co2)+" "+score_moisture(moisture)+" "+score_flora(flora);
            
        if (has_weed) score_msg += " There looks to be a strange alien plant on this planet that seems to want to eat us."
        if (has_fire) score_msg += " Also, is something burning? Break out the marshmallows!";
        
        score_msg +=
            "##Final score:  " + string_percent(score_final);
            
        //get surface image of planet
        var w = 800;
        var h = 800;
        var xx = w div 2;
        var yy = h div 2;
        var r = 250;
        var surf0 = surface_create(w, h);
        surface_set_target(surf0);
        with(obj_planet)
        {
            draw_set_color(image_blend);
            draw_sprite_ext(s_flare, 0, xx, yy, atmos_scale/2, atmos_scale/2, 0, atmos_color, atmos_alpha);
            draw_sprite_ext(s_clouds, 0, xx, yy, atmos_scale/2, atmos_scale/2, clouds_angle, c_white, clouds_alpha);
            if (surface_exists(surf)) 
            {
                draw_surface_ext(surf, xx-surf_size/2, yy-surf_size/2, image_xscale, image_yscale, image_angle, image_blend, image_alpha);
            }
            else
            {
                draw_set_circle_precision(64);
                draw_circle(xx, yy, 2+radius/2, false);
                draw_set_circle_precision(24);
            }
        }       
        with(obj_lake)
        {
            x = xx+lengthdir_x(pos_r/2, pos_deg);
            y = yy+lengthdir_y(pos_r/2, pos_deg);
            shader_set(shd_lakefill);
            var shd_fill = shader_get_uniform(shd_lakefill, "Fill");
            shader_set_uniform_f(shd_fill, fill);
            draw_sprite_ext(sprite_index, image_index, x, y, image_xscale/2, image_yscale/2, image_angle, image_blend, image_alpha);
            shader_reset();
        }
        with(radial_interact)
        {
            x = xx+lengthdir_x(r, pos_deg);
            y = yy+lengthdir_y(r, pos_deg);
            draw_sprite_ext(sprite_index, image_index, x, y, image_xscale/2, image_yscale/2, image_angle, image_blend, image_alpha);
        }
        surface_reset_target();
        
        surface_save(surf0, "planetoid.png");
        
        planet_sprite = sprite_create_from_surface(surf0, 0, 0, w, h, true, false, xx, yy);
        
        surface_free(surf0);
    }   
}
