///string = score_flora(score);
{
    if (argument0 > 0.90)
    {
        return "The ecosystem is robust and thriving! Did you cheat with miracle grow?";
    }
    if (argument0 > 0.70)
    {
        return "The ecosystem is doing great! Colonists will enjoy easy gardening.";
    }
    if (argument0 > 0.40)
    {
        return "The ecosystem is solid but not excellent. At least there will be less mosquitos.";
    }
    if (argument0 > 0.20)
    {
        return "The ecosystem seems anemic. The colonists contemplate supplement using hydroponics.";
    }
    
    return "This place is still a barren rock. What have you been doing this whole time?"

}
