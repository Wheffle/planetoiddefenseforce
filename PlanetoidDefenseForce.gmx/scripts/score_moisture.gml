///string = score_moisture(score);
{
    if (argument0 > 0.90)
    {
        return "The humidity is killer, but there is plenty of water!";
    }
    if (argument0 > 0.70)
    {
        return "The colonists will enjoy abundant water.";
    }
    if (argument0 > 0.40)
    {
        return "There is more than enough water to survive but the colonists must ration.";
    }
    if (argument0 > 0.20)
    {
        return "The amount of water on this planet is pitiful, it will cause hardship and quarrels.";
    }
    
    return "This place is a desert; the colony will need to ban showering."

}
