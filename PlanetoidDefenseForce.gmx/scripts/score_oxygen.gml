///string = score_oxygen(score);
{
    if (argument0 > 0.30)
    {
        return "Due to the ridiculous level of oxygen, all manufacturing must use stainless steel and fireworks are strictly banned.";
    }
    if (argument0 > 0.15)
    {
        return "The oxygen levels are well-balanced.";
    }
    if (argument0 > 0.05)
    {
        return "Oxygen levels are rather low, only hardened mountain climbers will feel at home.";
    }
    
    return "With hardly any oxygen in the air, life will be a little awkward."

}
