///set_emergency_message(msg);
{
    with(ctrl_tutorial)
    {
        msg_emergency = argument0;
        show_emergency = true;
        emergency_timer = room_speed*20;
    }
}
