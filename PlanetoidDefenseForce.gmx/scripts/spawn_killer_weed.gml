///id = spawn_killer_weed(planet, pos_deg);
{
    var weed = instance_create_radial(argument0, argument1, 0, obj_killerweed);
    with(weed)
    {
        var p = instance_place(x, y, obj_flora);
        if (p)
        { 
            with(p) 
            {
                planet.biomass_damage += (biomass-1);
                instance_destroy();
            }
        }
        
        if (place_meeting(x, y, radial_interact))
        { 
            instance_destroy();
            return noone;
        }
        else
        {
            repeat(2) psys_create_smoke(x, y, global.c_brown);
            repeat(3) psys_create_smoke(x, y, c_green);
        }
    }
    return weed;
}
